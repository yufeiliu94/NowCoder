#pragma once

#include <vector>
#include <string>
#include <algorithm>
#include <optional>
#include <catch.hpp>

using namespace std;

namespace NowCoder {

template<typename T>
struct TreeNode {
  TreeNode(T value, TreeNode<T> *left, TreeNode<T> *right) : val{value}, left{left}, right{right} {}

  explicit TreeNode(T value) : val{value}, left{nullptr}, right{nullptr} {}

  bool is_leaf() const {
    return left == nullptr and right == nullptr;
  }

  static void release(TreeNode<T> *root) {
    if (root != nullptr) {
      release(root->left);
      release(root->right);
      delete root;
    }
  }

template<template<typename...> typename Container=std::vector>
static TreeNode<T> *build(const Container<std::optional<T>> &xs, int x) {
  if (x > xs.size()) {
    return nullptr;
  }
  const auto val = xs[x - 1];
  if (not val) {
    return nullptr;
  }
  return new TreeNode<T>(val.value(), build(xs, 2 * x), build(xs, 2 * x + 1));
}

static bool is_same(TreeNode<T> *lhs, TreeNode<T> *rhs) {
  if (lhs == rhs) {
    return true;
  }
  if (lhs == nullptr or rhs == nullptr) {
    return false;
  }
  return lhs->val == rhs->val and is_same(lhs->left, rhs->left) and is_same(lhs->right, rhs->right);
}

TreeNode *left, *right;
T val;
};

template<typename T>
struct Tree {
  template<template<typename> typename Container=std::vector>
  explicit Tree(const Container<std::optional<T>> &xs) : node{TreeNode<T>::build(xs, 1)} {}

  template<template<typename> typename Container=std::vector>
  explicit Tree(Container<std::optional<T>> &&xs) : node{TreeNode<T>::build(std::move(xs), 1)} {}

  Tree(std::initializer_list<std::optional<T>> il) : node{
      TreeNode<T>::build(std::vector<std::optional<T>>(il.begin(), il.end()), 1)} {}

  explicit Tree(TreeNode<T> *node) : node{node} {}

  ~Tree() {
    TreeNode<T>::release(node);
  }

  inline operator TreeNode<T> *() {
    return node;
  }

  inline TreeNode<T> *operator->() {
    return node;
  }

  bool operator==(const Tree<T> &rhs) const {
    TreeNode<T>::is_same(node, rhs.node);
  }

  TreeNode<T> *node;
};

template<typename T>
struct ListNode {
  explicit ListNode(T x) : val{x}, next{nullptr} {}
  ListNode(T x,  ListNode<T> *next) : val{x}, next{next} {}

  bool equalTo(const ListNode *rhs) const {
    auto lhs = this;
    while (lhs != nullptr and rhs != nullptr) {
      if (lhs->val != rhs->val) {
        return false;
      }
      lhs = lhs->next;
      rhs = rhs->next;
    }
    return lhs == nullptr and rhs == nullptr;
  }

  template<template<typename...> typename Container=std::vector>
  static ListNode<T> *build(const Container<T> &xs) {
    ListNode<T> *head = nullptr, *current = nullptr;
    for (auto iter = xs.cbegin(); iter != xs.cend(); iter++) {
      if (head == nullptr) {
        head = current = new ListNode<T>(*iter);
      } else {
        current->next = new ListNode<T>(*iter);
        current = current->next;
      }
    }
    return head;
  }

  static size_t size(ListNode<T> *xs) {
    size_t length = 0;
    while (xs != nullptr) {
      xs = xs->next;
      length += 1;
    }
    return length;
  }

  T val;
  ListNode *next;
};

template<typename T>
struct List {
  template<template<typename> typename Container=std::vector>
  explicit List(const Container<T> &xs) : node{ListNode<T>::build(xs)}{}

  template<template<typename> typename Container=std::vector>
  explicit List(const Container<T> &&xs) : node{ListNode<T>::build(std::move(xs))}{}

  explicit List(std::initializer_list<T> il) : node{ListNode<T>::build(std::vector<T>(il.begin(), il.end()))} {}

  inline operator ListNode<T> *() {
    return node;
  }

  std::vector<T> to_list() const {
    ListNode<T> *head = node;
    std::vector<T> xs;
    while (head != nullptr) {
      xs.push_back(head->val);
      head = head->next;
    }
    return xs;
  }

  ListNode<T> *node;
};

} // namespae NowCoder

using ListNode = NowCoder::ListNode<int>;
using List = NowCoder::List<int>;
using TreeNode = NowCoder::TreeNode<int>;
using Tree = NowCoder::Tree<int>;
