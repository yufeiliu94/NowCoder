/**
 * 输入某二叉树的前序遍历和中序遍历的结果，请重建出该二叉树。
 * 假设输入的前序遍历和中序遍历的结果中都不含重复的数字。
 * 例如输入前序遍历序列{1,2,4,7,3,5,6,8}和中序遍历序列{4,7,2,1,5,3,8,6}，则重建二叉树并返回。
 */

#include <vector>
#include <algorithm>
#include "nowcoder.hpp"

class Solution {
 public:
  using Range = std::pair<int, int>;
  TreeNode *reConstructBinaryTree(vector<int> &pre, vector<int> &vin, Range p, Range v) {
    if (p.first >= p.second || v.first >= v.second) {
      return nullptr;
    }
    // 根节点一定位于前序遍历首部
    auto root_value = pre[p.first];
    auto root = new TreeNode(root_value);
    // 确定根节点在中序遍历中的位置
    auto root_vin = find_root(vin, root_value, v);
    // 中序遍历序列中，root 的左子树中的元素一定在 [v.first, root_vin) 中, 共 lift_size = root_vin - v.first 个元素
    // 所以左子树的元素一定位于前序遍历序列的 [p.first + 1, p.first + 1 + left_size) 中
    auto left_size = root_vin - v.first;
    root->left = reConstructBinaryTree(pre, vin, {p.first + 1, p.first + 1 + left_size}, {v.first, root_vin});
    // 中序遍历序列中，root 的右子树中的元素一定在 [root_vin + 1, v.second) 中, 共 right_size = v.second - root_vin - 1 个元素
    // 所以左子树的元素一定位于前序遍历序列的 [p.first + 1 + left_size, p.second) 中
    auto right_size = v.second - root_vin - 1;
    root->right = reConstructBinaryTree(pre, vin, {p.first + 1 + left_size, p.second}, {root_vin + 1, v.second});
    return root;
  }

  static int find_root(const vector<int> &vin, const int value, Range range) {
    for (auto i = range.first; i < range.second; ++i) {
      if (vin[i] == value) {
        return i;
      }
    }
    return -1;
  }

  TreeNode *reConstructBinaryTree(vector<int> pre, vector<int> vin) {
    return reConstructBinaryTree(pre, vin, {0, pre.size()}, {0, vin.size()});
  }
};

#include <catch.hpp>

TEST_CASE("重建二叉树", "BinaryTree") {
  auto s = Solution();
  std::vector<int> ps = {1, 2, 4, 7, 3, 5, 6, 8}, vs{4, 7, 2, 1, 5, 3, 8, 6};
  auto rebuilt = s.reConstructBinaryTree(ps, vs);
  auto tree = Tree{1,
                   2, 3,
                   4, std::nullopt, 5, 6,
                   std::nullopt, 7, std::nullopt, std::nullopt, std::nullopt, std::nullopt, 8, std::nullopt};
  REQUIRE(TreeNode::is_same(rebuilt, tree));
}
