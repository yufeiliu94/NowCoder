/**
 * 输入一棵二叉树，求该树的深度。
 * NOTE: 从根结点到叶结点依次经过的结点（含根、叶结点）形成树的一条路径最长路径的长度为树的深度。
 */

#include "nowcoder.hpp"

class Solution {
 public:
  int TreeDepth(TreeNode *pRoot) {
    if (pRoot == nullptr) {
      return 0;
    } else {
      return 1 + std::max(TreeDepth(pRoot->left), TreeDepth(pRoot->right));
    }
  }
};

TEST_CASE("二叉树的深度", "Tree") {
  auto tree = Tree{1, 2, 3, 4, 5, std::nullopt, 6, std::nullopt, std::nullopt, 7};
  auto s = Solution();
  REQUIRE(s.TreeDepth(tree) == 4);
}
