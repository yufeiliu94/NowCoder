/**
 * 大家都知道斐波那契数列，现在要求输入一个整数n，请你输出斐波那契数列的第n项。 n<=39。
 * NOTE: 已探明 Fib(0) = 0
 */

#include <type_traits>
#include <cstdint>

template<uint64_t N>
struct Fib : std::integral_constant<uint64_t, Fib<N - 1>::value + Fib<N - 2>::value> {};

template<>
struct Fib<0> : std::integral_constant<uint64_t, 0> {};

template<>
struct Fib<1> : std::integral_constant<uint64_t, 1> {};

template<uint64_t ...N>
struct StaticFibImpl {
  static const uint64_t value[sizeof...(N)];
};

template<uint64_t ...N>
const uint64_t StaticFibImpl<N...>::value[] = {Fib<N>::value...};

template<uint64_t N, uint64_t ...NS>
struct StaticFib : StaticFib<N - 1, N, NS...> {};

template<uint64_t ...NS>
struct StaticFib<0, NS...> : StaticFibImpl<0, NS...> {};

class Solution {
 public:
  int Fibonacci(int n) {
    return StaticFib<39>::value[n];
  }
};

#include <catch.hpp>
#include <algorithm>

TEST_CASE("斐波那契数列","Math") {
  std::vector<int> fib(9), xs{0, 1, 1, 2, 3, 5, 8, 13, 21};
  std::copy_n(StaticFib<9>::value, 9, fib.begin());
  REQUIRE(fib == xs);
}
