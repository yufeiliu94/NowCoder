/**
 * 牛客最近来了一个新员工Fish，每天早晨总是会拿着一本英文杂志，写些句子在本子上。
 * 同事Cat对Fish写的内容颇感兴趣，有一天他向Fish借来翻看，但却读不懂它的意思。
 * 例如，“student. a am I”。后来才意识到，这家伙原来把句子单词的顺序翻转了，
 * 正确的句子应该是“I am a student.”。Cat对一一的翻转这些单词顺序可不在行，你能帮助他么？
 */

#include <vector>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <string>
#include <iterator>

class Solution {
 public:
  std::string ReverseSentence(std::string str) {
    std::vector<std::string> words;
    std::ostringstream os;
    // 分词并逐词压入 words
    auto text = new char[str.length() + 1];
    std::strncpy(text, str.c_str(), str.length() + 1);
    for (auto word = std::strtok(text, " "); word != nullptr; word = std::strtok(nullptr, " ")) {
      words.emplace_back(word);
    }
    delete[](text);
    if (words.empty()) {
      // NOTE: 经试错，如果输入中仅包含空格，须原样返回输入
      return str;
    } else {
      // 逆序复制到 os
      std::copy(words.crbegin(), words.crend() - 1, std::ostream_iterator<std::string>(os, " "));
      os << words.front();
    }
    return os.str();
  }
};

#include <catch.hpp>

TEST_CASE("翻转单词顺序列", "String") {
  auto s = Solution();
  REQUIRE(s.ReverseSentence("") == "");
  REQUIRE(s.ReverseSentence("w") == "w");
  REQUIRE(s.ReverseSentence("   ") == "   ");
  REQUIRE(s.ReverseSentence(" w ") == "w");
  REQUIRE(s.ReverseSentence("r  l") == "l r");
  REQUIRE(s.ReverseSentence("student. a am I") == "I am a student.");
}
