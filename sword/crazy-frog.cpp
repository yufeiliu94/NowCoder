/**
 * 一只青蛙一次可以跳上1级台阶，也可以跳上2级……它也可以跳上n级。求该青蛙跳上一个n级的台阶总共有多少种跳法。
 */

class Solution {
 public:
  int jumpFloorII(int number) {
    // 受制于现实意义和 int 的数值范围，[0, 32) 之外的 number 没有意义
    if (number <= 0 or number > 32) {
      return 0;
    }
    // f(n) = f(n - 1) + f(n - 2) + ... + f(0)
    //      = 2f(n - 1)
    // f(1) = 1 => f(n) = 2^(n - 1)
    return 1 << (number - 1);
  }
};

#include <catch.hpp>
#include <limits>

TEST_CASE("变态跳台阶", "Math") {
  auto s = Solution();
  auto m = std::numeric_limits<int>::min();
  REQUIRE(s.jumpFloorII(-1) == 0);
  REQUIRE(s.jumpFloorII(0) == 0);
  REQUIRE(s.jumpFloorII(1) == 1);
  REQUIRE(s.jumpFloorII(2) == 2);
  REQUIRE(s.jumpFloorII(3) == 4);
  REQUIRE(s.jumpFloorII(32) == m);
  REQUIRE(s.jumpFloorII(33) == 0);
}
