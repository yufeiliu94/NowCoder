/**
 * 用两个栈来实现一个队列，完成队列的Push和Pop操作。 队列中的元素为int类型。
 */

#include <stack>
using namespace std;

namespace queue_on_stack {

class Solution {
 public:
  void push(int node) {
    stack1.push(node);
  }

  int pop() {
    if (stack2.empty()) {
      while (not stack1.empty()) {
        stack2.push(stack1.top());
        stack1.pop();
      }
    }
    auto bottom = stack2.top();
    stack2.pop();
    return bottom;
  }

 private:
  stack<int> stack1;
  stack<int> stack2;
};

} // namespace queue_on_stack

#include <catch.hpp>

TEST_CASE("用两个栈实现队列", "Stack") {
  auto stack = queue_on_stack::Solution();
  std::vector<int> xs{1, 2, 3, 4, 5}, ys;
  stack.push(1);
  stack.push(2);
  ys.push_back(stack.pop());
  stack.push(3);
  ys.push_back(stack.pop());
  stack.push(4);
  stack.push(5);
  ys.push_back(stack.pop());
  ys.push_back(stack.pop());
  ys.push_back(stack.pop());
  REQUIRE(xs == ys);
}
