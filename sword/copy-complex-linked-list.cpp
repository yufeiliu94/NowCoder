/**
 * 输入一个复杂链表（每个节点中有节点值，以及两个指针，一个指向下一个节点，另一个特殊指针指向任意一个节点），
 * 返回结果为复制后复杂链表的head。（注意，输出结果中请不要返回参数中的节点引用，否则判题程序会直接返回空）
 */

#include <set>
#include "nowcoder.hpp"

struct RandomListNode {
  int label;
  struct RandomListNode *next, *random;

  RandomListNode(int x) :
      label(x), next(NULL), random(NULL) {
  }

  static bool is_same(RandomListNode *lhs, RandomListNode *rhs) {
    std::set<RandomListNode *> visit;
    return is_same(lhs, rhs, visit);
  }

 private:
  static bool is_same(RandomListNode *lhs, RandomListNode *rhs, std::set<RandomListNode *> &visit) {
    if (lhs == nullptr and rhs == nullptr) {
      return true;
    }
    if (lhs == nullptr or rhs == nullptr) {
      return false;
    }
    auto lhs_has_been_visited = visit.find(lhs) != visit.end();
    auto rhs_has_been_visited = visit.find(rhs) != visit.end();
    if (lhs_has_been_visited xor rhs_has_been_visited) {
      return false;
    }
    if (lhs_has_been_visited and rhs_has_been_visited) {
      return true;
    }
    if (lhs->label != rhs->label) {
      return false;
    }
    visit.insert(lhs);
    visit.insert(rhs);
    return is_same(lhs->random, rhs->random, visit);
  }
};

#include <map>

class Solution {
 public:
  RandomListNode *Clone(RandomListNode *head) {
    std::map<RandomListNode *, RandomListNode *> nodes;
    return CloneNodes(head, nodes);
  }

 private:
  RandomListNode *CloneNodes(RandomListNode *node, std::map<RandomListNode *, RandomListNode *> &nodes) {
    if (node == nullptr) {
      return nullptr;
    }
    auto has_been_cloned = nodes.find(node);
    // 若 node 曾被复制过，即原链表中存在环状结构，直接返回先前复制的结果
    // 否则创建 node 节点的副本 cloned，并递归复制 node->random 和 node->next
    if (has_been_cloned != nodes.end()) {
      return has_been_cloned->second;
    } else {
      auto cloned = new RandomListNode(node->label);
      nodes[node] = cloned;
      cloned->next = CloneNodes(node->next, nodes);
      cloned->random = CloneNodes(node->random, nodes);
      return cloned;
    }
  }
};

TEST_CASE("复杂链表的复制", "LinkedList") {
  auto lhs = new RandomListNode(1);
  auto rhs = new RandomListNode(1);
  lhs->random = lhs; lhs->next = rhs;
  rhs->random = rhs; rhs->next = lhs;
  REQUIRE(RandomListNode::is_same(lhs, rhs));
  auto s = Solution();
  auto clone = s.Clone(lhs);
  REQUIRE(RandomListNode::is_same(lhs, rhs));
  REQUIRE(RandomListNode::is_same(lhs, clone));
  REQUIRE(s.Clone(nullptr) == nullptr);
}
