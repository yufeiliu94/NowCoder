/**
 * 我们可以用2*1的小矩形横着或者竖着去覆盖更大的矩形。
 * 请问用n个2*1的小矩形无重叠地覆盖一个2*n的大矩形，总共有多少种方法？
 */

#include <type_traits>
#include <cstdint>

template<uint64_t N>
struct Fib : std::integral_constant<uint64_t, Fib<N - 1>::value + Fib<N - 2>::value> {};

template<>
struct Fib<0> : std::integral_constant<uint64_t, 1> {};

template<>
struct Fib<1> : std::integral_constant<uint64_t, 1> {};

template<uint64_t ...N>
struct StaticFibImpl {
  static const uint64_t value[sizeof...(N)];
};

template<uint64_t ...N>
const uint64_t StaticFibImpl<N...>::value[] = {Fib<N>::value...};

template<uint64_t N, uint64_t ...NS>
struct StaticFib : StaticFib<N - 1, N, NS...> {};

template<uint64_t ...NS>
struct StaticFib<0, NS...> : StaticFibImpl<0, NS...> {};

class Solution {
 public:
  /**
   * 大小为 (2, n) 的矩形，
   * 1. 可以在 (2, n-2) 的矩形的基础上添加 2 个横向的 (2, 1) 矩形得到
   * 2. 可以在 (2, n-1) 的矩形的基础上添加 1 个纵向的 (2, 1) 矩形得到
   * 所以 f(n) = f(n - 1) + f(n - 2) = Fib(n)
   */
  int rectCover(int number) {
    if (number <= 0 or number > 38) {
      return 0;
    }
    return StaticFib<39>::value[number];
  }
};

#include <catch.hpp>

TEST_CASE("矩形覆盖", "Math") {
  auto s = Solution();
  REQUIRE(s.rectCover(1) == 1);
  REQUIRE(s.rectCover(2) == 2);
  REQUIRE(s.rectCover(3) == 3);
  REQUIRE(s.rectCover(4) == 5);
}
