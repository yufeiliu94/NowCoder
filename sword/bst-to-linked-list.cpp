/**
 * 输入一棵二叉搜索树，将该二叉搜索树转换成一个排序的双向链表。要求不能创建任何新的结点，只能调整树中结点指针的指向。
 */

#include "nowcoder.hpp"

class Solution {
 public:
  TreeNode *Convert(TreeNode *root) {
    if (root == nullptr) {
      return nullptr;
    }
    auto less = Convert(root->left);
    auto greater = Convert(root->right);
    if (greater != nullptr) {
      root->right = greater;
      greater->left = root;
    }
    greater = root;
    return connect(less, greater);
  }

 private:
  TreeNode *connect(TreeNode *lhs, TreeNode *rhs) {
    if (lhs == nullptr) {
      return rhs;
    }
    auto cursor = lhs;
    while (cursor->right != nullptr) {
      cursor = cursor->right;
    }
    cursor->right = rhs;
    rhs->left = cursor;
    return lhs;
  }
};

auto vectorize(TreeNode *head) {
  std::vector<int> forward, backward;
  auto cursor = head;
  while (cursor->right != nullptr) {
    forward.push_back(cursor->val);
    cursor = cursor->right;
  }
  forward.push_back(cursor->val);
  do {
    backward.push_back(cursor->val);
    cursor = cursor->left;
  } while (cursor != nullptr);
  return std::make_tuple(forward, backward);
};

TEST_CASE("二叉搜索树与双向链表", "Tree") {
  auto s = Solution();
  auto bst = new Tree{4, 2, 6, 1, nullopt, nullopt, 7};
  std::vector xs{1, 2, 4, 6, 7};
  auto ys = decltype(xs)(xs.crbegin(), xs.crend());
  auto[forward, backward] = vectorize(s.Convert(*bst));
  REQUIRE(forward == xs);
  REQUIRE(backward == ys);
  REQUIRE(s.Convert(nullptr) == nullptr);
}
