/**
 * 输入两个整数序列，第一个序列表示栈的压入顺序，请判断第二个序列是否为该栈的弹出顺序。
 * 假设压入栈的所有数字均不相等。例如序列1,2,3,4,5是某栈的压入顺序，
 * 序列4,5,3,2,1是该压栈序列对应的一个弹出序列，但4,3,5,1,2就不可能是该压栈序列的弹出序列。
 * 注意：这两个序列的长度是相等的
 */

#include <vector>
#include <stack>

class Solution {
 public:
  bool IsPopOrder(std::vector<int> pushV, std::vector<int> popV) {
    std::stack<int> emu;
    auto pop_pos = 0, push_pos = 0;
    // 如果出入序列长度不等，一定不可能
    if (pushV.size() != popV.size()) {
      return false;
    }
    // 将非乱序出栈的元素压入寄存栈
    while (push_pos < pushV.size() and pop_pos < popV.size()) {
      if (pushV[push_pos] != popV[pop_pos]) {
        emu.push(pushV[push_pos]);
        push_pos += 1;
      } else {
        pop_pos += 1;
        push_pos += 1;
      }
    }
    // 将寄存栈中的元素出栈，直到失配
    while (not emu.empty() and pop_pos < popV.size()) {
      if (emu.top() == popV[pop_pos]) {
        emu.pop();
        pop_pos += 1;
      } else {
        break;
      }
    }
    // 出栈顺序合法 <=> 寄存栈为空
    return emu.empty();
  }
};

#include <catch.hpp>

TEST_CASE("栈的压入弹出序列", "Stack") {
  auto s = Solution();
  std::vector in{1, 2, 3, 4, 5}, yes{4, 5, 3, 2, 1}, no{4, 3, 5, 1, 2};
  REQUIRE(s.IsPopOrder(in, in));
  REQUIRE(s.IsPopOrder(in, yes));
  REQUIRE(s.IsPopOrder(in, {5, 4, 3, 2, 1}));
  REQUIRE_FALSE(s.IsPopOrder(in, no));
  REQUIRE_FALSE(s.IsPopOrder(in, {}));
}
