/**
 * 一只青蛙一次可以跳上1级台阶，也可以跳上2级。求该青蛙跳上一个n级的台阶总共有多少种跳法。
 */

#include <type_traits>
#include <cstdint>
#include <stdexcept>

template<uint64_t N>
struct Fib : std::integral_constant<uint64_t, Fib<N - 1>::value + Fib<N - 2>::value> {};

template<>
struct Fib<0> : std::integral_constant<uint64_t, 1> {};

template<>
struct Fib<1> : std::integral_constant<uint64_t, 1> {};

template<uint64_t ...N>
struct StaticFibImpl {
  static const uint64_t value[sizeof...(N)];
};

template<uint64_t ...N>
const uint64_t StaticFibImpl<N...>::value[] = {Fib<N>::value...};

template<uint64_t N, uint64_t ...NS>
struct StaticFib : StaticFib<N - 1, N, NS...> {};

template<uint64_t ...NS>
struct StaticFib<0, NS...> : StaticFibImpl<0, NS...> {};

class Solution {
 public:
  int jumpFloor(int number) {
    if (number < 0 or number > 39) {
      throw std::invalid_argument("Excepted 0 <= number <= 39, got: " + std::to_string(number));
    }
    return StaticFib<39>::value[number];
  }
};

#include <catch.hpp>
#include <algorithm>

TEST_CASE("跳台阶", "Math") {
  std::vector<int> fib(8), xs{1, 1, 2, 3, 5, 8, 13, 21};
  std::copy_n(StaticFib<8>::value, 8, fib.begin());
  REQUIRE(fib == xs);
}
