/**
 * 把一个数组最开始的若干个元素搬到数组的末尾，我们称之为数组的旋转。
 * 输入一个非递减排序的数组的一个旋转，输出旋转数组的最小元素。
 * 例如数组{3,4,5,1,2}为{1,2,3,4,5}的一个旋转，该数组的最小值为1。
 * NOTE：给出的所有元素都大于0，若数组大小为0，请返回0。
 */

#include <vector>
using namespace std;

class Solution {
 public:
  int minNumberInRotateArray(vector<int> rotateArray) {
    if (rotateArray.empty()) {
      return 0;
    }
    int threshold = rotateArray[0];
    for (auto i = 1; i < rotateArray.size(); ++i) {
      if (threshold > rotateArray[i]) {
        return rotateArray[i];
      } else {
        threshold = rotateArray[i];
      }
    }
    return rotateArray[0];
  }
};

#include <catch.hpp>

TEST_CASE("旋转数组的最小数字", "Search") {
  std::vector<int> xs{1, 2, 3, 4, 5}, ys{3, 4, 5, 1, 2};
  auto s = Solution();
  REQUIRE(s.minNumberInRotateArray(xs) == 1);
  REQUIRE(s.minNumberInRotateArray(ys) == 1);
}
