/**
 * 操作给定的二叉树，将其变换为源二叉树的镜像。
 */

#include "nowcoder.hpp"

class Solution {
 public:
  void Mirror(TreeNode *root) {
    if (root != nullptr) {
      std::swap(root->left, root->right);
      Mirror(root->left);
      Mirror(root->right);
    }
  }
};

TEST_CASE("二叉树的镜像", "Tree") {
  Tree tree{8, 6, 10, 5, 7, 9, 11}, mirror{8, 10, 6, 11, 9, 7, 5};
  Solution().Mirror(tree);
  REQUIRE(TreeNode::is_same(tree, mirror));
}
