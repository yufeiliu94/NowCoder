# encoding: UTF-8

"""
# 《剑指Offer》在线编程题

| 序号 | 题目 | 标签 | 路径 |
| ---- | ---- | ---- | ---- |@
{% for i, solution in solutions %}
| {{i}} | {{solution.name}} | {{solution.tag}} | {{solution.relpath}} |@
{% endfor %}
"""

import os
import re
from jinja2 import Template
from collections import namedtuple

Solution = namedtuple('Solution', ['relpath', 'name', 'tag', 'note'])


def read_cpp(path):
    name, tag, note = None, None, None
    with open(path) as cpp:
        source = cpp.readlines()
        note = ''.join(line[3:] for line in source[1:source.index(' */\n')])
        for line in source:
            case = re.match(r'TEST_CASE\("(.*)",\s*"(.*)"\)', line)
            if case is not None:
                name, tag = case.groups()
        return name, tag, note.strip()


def readme():
    solutions = []
    sword = os.path.dirname(os.path.abspath(__file__))
    root = os.path.dirname(sword)
    for term in os.listdir(sword):
        if re.match(r'.*\.cpp$', term):
            cpp = os.path.join(sword, term)
            name, tag, note = read_cpp(cpp)
            relpath = os.path.relpath(cpp, start=root)
            solutions.append(Solution(relpath, name, tag, note))
    tpl = __doc__.strip()
    xs = enumerate(solutions, start=1)
    markdown = ''.join(w.replace('@\n', '') for w in Template(tpl).generate(solutions=xs))
    print(markdown)


if __name__ == '__main__':
    readme()
