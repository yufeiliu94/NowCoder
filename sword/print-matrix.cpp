/**
 * 输入一个矩阵，按照从外向里以顺时针的顺序依次打印出每一个数字，例如，
 * 如果输入如下矩阵： 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 则依次打印出数字1,2,3,4,8,12,16,15,14,13,9,5,6,7,11,10.
 */

#include "nowcoder.hpp"

class Solution {
 public:
  vector<int> printMatrix(vector<vector<int>> matrix) {
    vector<int> os;
    for (auto rows = matrix.size(); rows != 0; rows = matrix.size()) {
      size_t cols = matrix[0].size();
      // 矩阵为空时，过程结束
      if (rows * cols == 0) {
        return os;
      }
      // 处理行向量
      if (rows == 1) {
        os.insert(os.end(), matrix[0].begin(), matrix[0].end());
        return os;
      }
      // 处理列向量
      if (cols == 1) {
        for (auto &m : matrix) {
          os.push_back(m[0]);
        }
        return os;
      }
      // 打印复制第一行
      os.insert(os.end(), matrix[0].cbegin(), matrix[0].cend());
      matrix.erase(matrix.begin());
      // 打印第 cols 列
      for (size_t i = 0; i + 2 < rows; ++i) {
        os.push_back(matrix[i][cols - 1]);
        matrix[i].pop_back();
      }
      // 逆序打印最后一行
      if (rows >= 2) {
        os.insert(os.end(), matrix[rows - 2].crbegin(), matrix[rows - 2].crend());
        matrix.erase(matrix.end() - 1);
      }
      // 逆序打印第 1 列
      for (ssize_t i = rows - 3; i >= 0; --i) {
        os.push_back(matrix[i][0]);
        matrix[i].erase(matrix[i].begin());
      }
    }
    return os;
  }
};

TEST_CASE("顺时针打印矩阵", "Misc") {
  std::vector<std::vector<int>> matrix{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};
  std::vector<int> print{1, 2, 3, 4, 8, 12, 16, 15, 14, 13, 9, 5, 6, 7, 11, 10}, empty{};
  std::vector<int> xs{1, 2, 3, 6, 9, 8, 7, 4, 5}, ys{1, 2, 3, 4};
  auto s = Solution();
  REQUIRE(s.printMatrix(matrix) == print);
  REQUIRE(s.printMatrix({}) == empty);
  REQUIRE(s.printMatrix({{}, {}, {}}) == empty);
  REQUIRE(s.printMatrix({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}) == xs);
  REQUIRE(s.printMatrix({{1}, {2}, {3}, {4}}) == ys);
  REQUIRE(s.printMatrix({{1, 2, 3, 4}}) == ys);
}
