/**
 * 给定一个double类型的浮点数base和int类型的整数exponent。求base的exponent次方。
 */

#include <cstdlib>

class Solution {
 public:
  double Power(double base, int exponent) {
    if (exponent == 0) {
      return 1.;
    }
    if (exponent == 1) {
      return base;
    }
    if (exponent < 0) {
      return 1. / Power(base, -exponent);
    }
    auto q = std::div(exponent, 2);
    double power = Power(base, q.quot);
    power *= power;
    if (q.rem) {
      power *= base;
    }
    return power;
  }
};

#include <cmath>
#include <catch.hpp>

TEST_CASE("数值的整数次方", "Math") {
  auto s = Solution();
  auto pi = 3.1415926535897932384626433832795 / 1e4;
  REQUIRE(s.Power(pi, 0) == Approx(std::pow(pi, 0)));
  REQUIRE(s.Power(pi, 1) == Approx(std::pow(pi, 1)));
  REQUIRE(s.Power(pi, 2) == Approx(std::pow(pi, 2)));
  REQUIRE(s.Power(pi, 5) == Approx(std::pow(pi, 5)));
  REQUIRE(s.Power(pi, -1) == Approx(std::pow(pi, -1)));
  REQUIRE(s.Power(pi, -2) == Approx(std::pow(pi, -2)));
  REQUIRE(s.Power(pi, -5) == Approx(std::pow(pi, -5)));
}
