/**
 * 输入一个递增排序的数组和一个数字S，在数组中查找两个数，是的他们的和正好是S，如果有多对数字的和等于S，输出两个数的乘积最小的。
 */

#include <vector>
#include <limits>
#include <cstdint>
#include <algorithm>

class Solution {
 public:
  std::vector<int> FindNumbersWithSum(std::vector<int> array, int sum) {
    bool found = false;
    int64_t product = std::numeric_limits<int64_t>::max();
    int lhs = 0, rhs = sum;
    const auto size = array.size();
    for (size_t i = 1; i < size; ++i) {
      auto current = array[i - 1];
      auto another_index = binary_search(array, sum - array[i - 1], i, size);
      if (another_index < size and (current * array[another_index] < product or not found)) {
        lhs = current;
        rhs = array[another_index];
        product = lhs * rhs;
        found = true;
      }
    }
    if (not found) {
      return std::vector<int>{};
    } else {
      if (lhs > rhs) {
        std::swap(lhs, rhs);
      }
      return std::vector<int>{lhs, rhs};
    }
  }
 private:
  /**
   * 在 xs 中下标 [begin, end) 的范围内搜索 target
   * @return 若 target 存在于 xs 的 [begin, end) 范围中，返回其下标；否则返回 end
   */
  size_t binary_search(const std::vector<int> &xs, int value, size_t begin, size_t end) {
    if (begin >= end) {
      return end;
    }
    size_t mid = (begin + end) / 2;
    if (xs[mid] == value) {
      return mid;
    } else if (xs[mid] > value) {
      const auto index = binary_search(xs, value, begin, mid);
      return index == mid ? end : index;
    } else {
      return binary_search(xs, value, mid + 1, end);
    }
  }
};

#include <catch.hpp>

TEST_CASE("和为S的两个数字", "Algorithm") {
  std::vector<int> xs{-2, 0, 2, 3, 4, 5}, zero{-2, 2}, five{0, 5}, seven{2, 5}, empty{};
  std::vector<int> ys{1, 2, 4, 7, 11, 15}, fourteen{4, 11};
  auto s = Solution();
  REQUIRE(s.FindNumbersWithSum(xs, 0) == zero);
  REQUIRE(s.FindNumbersWithSum(xs, 5) == five);
  REQUIRE(s.FindNumbersWithSum(xs, 7) == seven);
  REQUIRE(s.FindNumbersWithSum(xs, 10) == empty);
  REQUIRE(s.FindNumbersWithSum(ys, 15) == fourteen);
}
