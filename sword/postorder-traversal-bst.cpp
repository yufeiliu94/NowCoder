/**
 * 输入一个整数数组，判断该数组是不是某二叉搜索树的后序遍历的结果。如果是则输出Yes,否则输出No。
 * 假设输入的数组的任意两个数字都互不相同。
 */

#include <vector>
#include <tuple>

class Solution {
 public:
  bool VerifySquenceOfBST(std::vector<int> sequence) {
    // 完全空节点
    if (sequence.empty()) {
      return false;
    }
    return verify(sequence, 0, sequence.size());
  }
 private:
  /**
   * 验证BST的合法性计算左右子树的分界点
   */
  std::tuple<bool, size_t>
  find_pivot(std::vector<int> &seq, int root, size_t begin, size_t end) {
    size_t cursor = begin, pivot = end;
    // 假设首段连续的、小于根节点的元素构成左子树
    for (; cursor < end; ++cursor) {
      if (seq[cursor] > root) {
        break;
      }
    }
    pivot = cursor;
    // 如果有子树中存在比根节点更小的元素，则序列非法
    for (; cursor < end; ++cursor) {
      if (seq[cursor] < root) {
        return std::make_tuple(false, end);
      }
    }
    return std::make_tuple(true, pivot);
  }

  bool verify(std::vector<int> &seq, size_t begin, size_t end) {
    // 叶子节点 / 空节点必定是合法的 BST
    if (begin >= end) {
      return true;
    }
    auto root = seq[end - 1];
    bool legal;
    size_t pivot;
    std::tie(legal, pivot) = find_pivot(seq, root, begin, end - 1);
    // 递归检查左右子树
    return legal and verify(seq, begin, pivot) and verify(seq, pivot, end - 1);
  }
};

#include <catch.hpp>

TEST_CASE("二叉搜索树的后序遍历序列", "Tree") {
  auto s = Solution();
  REQUIRE(s.VerifySquenceOfBST({1}));
  REQUIRE(s.VerifySquenceOfBST({1, 2, 3}));
  REQUIRE(s.VerifySquenceOfBST({1, 3, 2, 5, 7, 6, 4}));
  REQUIRE_FALSE(s.VerifySquenceOfBST({}));
  REQUIRE_FALSE(s.VerifySquenceOfBST({1, 3, 8, 5, 7, 6, 4}));
}
