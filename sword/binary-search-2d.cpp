/**
 * 在一个二维数组中，每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。
 * 请完成一个函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
 */

#include "nowcoder.hpp"

class Solution {
 public:
  bool Find(int target, vector<vector<int>> array) {
    for (const auto &xs : array) {
      if (binary_search(xs.cbegin(), xs.cend(), target)) {
        return true;
      }
    }
    return false;
  }
};

TEST_CASE("二维数组中的查找", "Search") {
  auto s = Solution();
  REQUIRE_FALSE(s.Find(0, {}));
  REQUIRE_FALSE(s.Find(4, {{1, 3}, {2, 5}}));
  REQUIRE(s.Find(4, {{1, 3, 5}, {2, 4, 6}}));
}
