/**
 * 定义栈的数据结构，请在该类型中实现一个能够得到栈最小元素的min函数。
 */

#include <algorithm>
#include <limits>

namespace stack_with_min {

class Solution {
 public:
  Solution() : pos(0), allocated(16), stack(new int[16]), minimal(new int[16]) {
    minimal[0] = std::numeric_limits<int>::max();
  }
  ~Solution() {
    delete[](stack);
    delete[](minimal);
  }

  void push(int value) {
    // 如果已分配内存耗尽，则重新分配
    if (pos + 1 >= allocated) {
      stack = realloc(stack, allocated, allocated * 2);
      minimal = realloc(minimal, allocated, allocated * 2);
      allocated += allocated;
    }
    // 压栈 value / 最小值
    pos += 1;
    stack[pos] = value;
    minimal[pos] = std::min(minimal[pos - 1], value);
  }

  void pop() {
    // 对空栈执行弹栈操作的行为未定义
    if (pos > 0) {
      pos -= 1;
    }
  }

  int top() const {
    return stack[pos];
  }

  int min() const {
    return minimal[pos];
  }

 private:
  int *realloc(int *p, size_t original, size_t target) {
    auto mem = new int[target];
    std::copy_n(p, original, mem);
    delete[](p);
    return mem;
  }

  int *stack, *minimal;
  size_t allocated, pos;
};

} // namespace stack_with_min

#include <catch.hpp>

TEST_CASE("包含min函数的栈", "Implementation") {
  auto stack = stack_with_min::Solution();
  for (auto x = 42; x > 0; --x) {
    stack.push(x);
  }
  for (auto x = 1; x <= 42; ++x) {
    REQUIRE(stack.min() == x);
    REQUIRE(stack.top() == x);
    stack.pop();
  }
}
