/**
 * 输入两个单调递增的链表，输出两个链表合成后的链表，当然我们需要合成后的链表满足单调不减规则。
 */

#include "nowcoder.hpp"

class Solution {
 public:
  ListNode *Merge(ListNode *pHead1, ListNode *pHead2) {
    // 处理输入链表中存在空表的情况
    if (pHead1 == nullptr) {
      return pHead2;
    }
    if (pHead2 == nullptr) {
      return pHead1;
    }
    // 以输入中最小的元素作为新链表的首部
    if (pHead1->val > pHead2->val) {
      std::swap(pHead1, pHead2);
    }
    auto head = pHead1;
    pHead1 = pHead1->next;
    // 合并输入直至至少有一个输入被完全并入新链表
    auto cursor = head;
    while (pHead1 != nullptr and pHead2 != nullptr) {
      if (pHead1->val < pHead2->val) {
        cursor->next = pHead1;
        pHead1 = pHead1->next;
      } else {
        cursor->next = pHead2;
        pHead2 = pHead2->next;
      }
      cursor = cursor->next;
    }
    // 将剩余节点顺序合并入新链表
    Extend(cursor, pHead1 ? pHead1 : pHead2);
    return head;
  }
 private:
  void Extend(ListNode *xs, ListNode *ys) {
    while (ys != nullptr) {
      xs->next = ys;
      ys = ys->next;
    }
  }
};

TEST_CASE("合并两个排序的链表", "LinkedList") {
  auto s = Solution();
  List xs{1, 3, 5, 7}, ys{2, 4, 6}, zs{1, 2, 3, 4, 5, 6, 7};
  REQUIRE(s.Merge(xs, ys)->equalTo(zs));
  REQUIRE(s.Merge(xs, nullptr) == (ListNode *) (xs));
  REQUIRE(s.Merge(nullptr, xs) == (ListNode *) (xs));
  REQUIRE(s.Merge(nullptr, nullptr) == nullptr);
}
