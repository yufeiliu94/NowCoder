/**
 * 输入一个整数，输出该数二进制表示中1的个数。其中负数用补码表示。
 */

class Solution {
 public:
  int  NumberOf1(int n) {
    int bits = 0;
    while (n != 0) {
      n &= n - 1;
      bits += 1;
    }
    return bits;
  }
};

#include <catch.hpp>
#include <bitset>

size_t standard_answer(int n) {
  return std::bitset<32>{uint64_t(n)}.count();
}

TEST_CASE("二进制中1的个数", "Bits") {
  auto s = Solution();
  REQUIRE(s.NumberOf1(0) == standard_answer(0));
  REQUIRE(s.NumberOf1(1) == standard_answer(1));
  REQUIRE(s.NumberOf1(-1) == standard_answer(-1));
}
