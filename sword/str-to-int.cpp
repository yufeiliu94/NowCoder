/**
 * 将一个字符串转换成一个整数，要求不能使用字符串转换整数的库函数。
 * 数值为0或者字符串不是一个合法的数值则返回0
 */

#include <string>
#include <limits>
#include <functional>

class Solution {
 public:
  int StrToInt(const std::string str) {
    int64_t value = 0LL;
    const int64_t bound = 1LL << 31;
    bool negate = false;
    size_t cursor = 0;
    // 跳过前导空白
    skip_any(' ', str, cursor);
    // 处理正负号, 默认允许连续的正负号
    for (; cursor < str.length(); ++cursor) {
      const auto s = str[cursor];
      if (s == '-') {
        negate = not negate;
      } else if (s != '+') {
        break;
      }
    }
    // 允许符号指示与数位之间存在空格
    skip_any(' ', str, cursor);
    // 逐位读取数字
    while (cursor < str.length()) {
      const auto digit = str[cursor++];
      if (std::isdigit(digit) and value < bound) {
        value = value * 10 + digit - '0';
      } else {
        return 0;
      }
    }
    // 所有数字位之后仅允许存在空格
    skip_any(' ', str, cursor);
    if (cursor != str.length() or value > bound) {
      return 0;
    }
    // 处理数值边界
    if (negate) {
      return static_cast<int>(-value);
    } else {
      if (value == bound) {
        return 0;
      } else {
        return static_cast<int>(value);
      }
    }
  }
 private:
  inline void skip_any(char excepted, const std::string &str, size_t &cursor) {
    for (; cursor < str.length() and str[cursor] == excepted; ++cursor);
  }
};

#include <catch.hpp>

TEST_CASE("把字符串转换成整数", "Implementation") {
  auto s = Solution();
  REQUIRE(s.StrToInt("") == 0);
  REQUIRE(s.StrToInt("+") == 0);
  REQUIRE(s.StrToInt("-") == 0);
  REQUIRE(s.StrToInt("0") == 0);
  REQUIRE(s.StrToInt("+0") == 0);
  REQUIRE(s.StrToInt("-0") == 0);
  REQUIRE(s.StrToInt("+-+-+-+-+") == 0);
  REQUIRE(s.StrToInt("2147483647") == 2147483647);
  REQUIRE(s.StrToInt("2147483648") == 0);
  REQUIRE(s.StrToInt("-2147483648") == -2147483648);
  REQUIRE(s.StrToInt("-2147483647") == -2147483647);
  REQUIRE(s.StrToInt(" 2147483647") == 2147483647);
  REQUIRE(s.StrToInt("+2147483647") == 2147483647);
  REQUIRE(s.StrToInt("-2147483648") == -2147483648);
  REQUIRE(s.StrToInt("-2147483648") == -2147483648);
  REQUIRE(s.StrToInt("+123-4") == 0);
}
