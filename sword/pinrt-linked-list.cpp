/**
 * 输入一个链表，从尾到头打印链表每个节点的值。
 */

#include <iostream>
#include <vector>
#include <stack>
#include "nowcoder.hpp"

using namespace std;

class Solution {
 public:
  vector<int> printListFromTailToHead(ListNode *head) {
    auto cursor = head;
    size_t length = 0;
    while (cursor != nullptr) {
      cursor = cursor->next;
      length += 1;
    }
    vector<int> reversed(length);
    cursor = head;
    while (cursor != nullptr) {
      reversed[--length] = cursor->val;
      cursor = cursor->next;
    }
    return reversed;
  }
};

TEST_CASE("从尾到头打印链表", "LinkedList") {
  auto s = Solution();
  std::vector<int> empty{}, ys{4, 3, 2, 1};
  auto xs = List{1, 2, 3, 4};
  REQUIRE(s.printListFromTailToHead(xs) == ys);
  REQUIRE(s.printListFromTailToHead(nullptr) == empty);
}
