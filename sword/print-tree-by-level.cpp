/**
 * 从上往下打印出二叉树的每个节点，同层节点从左至右打印。
 */

#include "nowcoder.hpp"

#include <vector>
#include <deque>

class Solution {
 public:
  std::vector<int> PrintFromTopToBottom(TreeNode *root) {
    std::vector<int> os;
    std::deque<TreeNode *> nodes{root};
    while (not nodes.empty()) {
      auto p = nodes.front();
      nodes.pop_front();
      if (p == nullptr) {
        continue;
      }
      os.push_back(p->val);
      nodes.push_back(p->left);
      nodes.push_back(p->right);
    }
    return os;
  }
};

TEST_CASE("从上往下打印二叉树", "Tree") {
  Tree tree{1, 2, 3, 4, nullopt, 5 ,6, 7};
  std::vector<int> xs{1, 2, 3, 4, 5 ,6, 7}, empty;
  auto s = Solution();
  REQUIRE(s.PrintFromTopToBottom(tree) == xs);
  REQUIRE(s.PrintFromTopToBottom(nullptr) == empty);
}
