/**
 * 写一个函数，求两个整数之和，要求在函数体内不得使用+、-、*、/四则运算符号。
 */

class Solution {
 public:
  /**
   * 模拟串行进位全加器
   * @param num1 被加数
   * @param num2 加数
   * @return 和
   */
  int Add(int num1, int num2) {
    int sum = 0;
    bool carry = false;
    for (auto i = 0; i < 8 * sizeof(int); ++i) {
      const int mask = 1 << i;
      const bool lhs = (mask & num1) != 0;
      const bool rhs = (mask & num2) != 0;
      // xor 相当于不考虑进位的二进制加法
      const bool bit = lhs xor rhs xor carry;
      // lhs 和 rhs 同时为 1 时一定会进位
      // lhs 和 rhs 中仅有一个为 1 时，当且仅当上一位相加产生了进位时才会进位
      // lhs 和 rhs 同时为 0 时，一定不会进位
      carry = (lhs and rhs) or ((lhs xor rhs) and carry);
      sum |= bit << i;
    }
    return sum;
  }
};

#include <catch.hpp>

TEST_CASE("不用加减乘除做加法", "Bits") {
  auto s = Solution();
  REQUIRE(s.Add(1, 2) == 3);
  REQUIRE(s.Add(1, -1) == 0);
  REQUIRE(s.Add(1, -2) == -1);
  REQUIRE(s.Add(-1, -2) == -3);
}
