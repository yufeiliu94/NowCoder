/**
 * 请实现一个函数，将一个字符串中的空格替换成“%20”。
 * 例如，当字符串为We Are Happy.则经过替换之后的字符串为We%20Are%20Happy。
 */

#include <cstring>
#include <sstream>
class Solution {
 public:
  void replaceSpace(char *str, int length) {
    std::stringbuf ss;
    for (auto i = 0; i < length; ++i) {
      if (str[i] == ' ') {
        ss.sputn("%20", 3);
      } else {
        ss.sputc(str[i]);
      }
    }
    auto s = ss.str();
    std::strncpy(str, s.c_str(), s.length() + 1);
  }
};

#include <catch.hpp>

TEST_CASE("替换空格", "String") {
  auto s = new char[9], r = new char[9];
  std::strncpy(s, "A B ", 5);
  std::strncpy(r, "A%20B%20", 9);
  Solution().replaceSpace(s, 4);
  REQUIRE(std::strcmp(s, r) == 0);
}
