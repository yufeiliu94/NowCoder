/**
 * 输入一个链表，输出该链表中倒数第k个结点。
 * NOTE: 计数从 1 开始，即倒数第 1 个元素为队尾元素
 */

#include "nowcoder.hpp"

class Solution {
 public:
  ListNode *FindKthToTail(ListNode *pListHead, unsigned int k) {
    unsigned int length = 0;
    auto kth = pListHead, cursor = pListHead;
    while (length < k) {
      if (cursor != nullptr) {
        length += 1;
        cursor = cursor->next;
      } else {
        return nullptr;
      }
    }
    while (cursor != nullptr) {
      cursor = cursor->next;
      kth = kth->next;
    }
    return kth;
  }
};

TEST_CASE("链表中倒数第k个结点", "LinkedList") {
  auto s = Solution();
  List xs{1, 2, 3, 4, 5, 6, 7, 8};
  REQUIRE(s.FindKthToTail(xs, 1)->val == 8);
  REQUIRE(s.FindKthToTail(xs, 2)->val == 7);
  REQUIRE(s.FindKthToTail(xs, 7)->val == 2);
  REQUIRE(s.FindKthToTail(xs, 8)->val == 1);
  REQUIRE(s.FindKthToTail(xs, 9) == nullptr);
  REQUIRE(s.FindKthToTail(xs, -1) == nullptr);
  REQUIRE(s.FindKthToTail(nullptr, 0) == nullptr);
}
