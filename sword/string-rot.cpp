/**
 * 汇编语言中有一种移位指令叫做循环左移（ROL），现在有个简单的任务，就是用字符串模拟这个指令的运算结果。
 * 对于一个给定的字符序列S，请你把其循环左移K位后的序列输出。例如，字符序列S=”abcXYZdef”,
 * 要求输出循环左移3位后的结果，即“XYZdefabc”。是不是很简单？OK，搞定它！
 */

#include "nowcoder.hpp"

class Solution {
 public:
  string LeftRotateString(string str, int n) {
    // 若输入字符串为空，循环左移结果总是空字符串
    if (str.length() == 0) {
      return str;
    }
    // 若位移量大于输入字符串长度，仅余数有效
    n %= str.length();
    // 若偏移量为 0 或 n 为负数，不必位移
    if (n <= 0) {
      return str;
    }
    return str.substr(n) + str.substr(0, n);
  }
};

TEST_CASE("左旋转字符串", "String") {
  auto s = Solution();
  REQUIRE(s.LeftRotateString("abcXYZdef"s, 0) == "abcXYZdef"s);
  REQUIRE(s.LeftRotateString("abcXYZdef"s, 3) == "XYZdefabc"s);
  REQUIRE(s.LeftRotateString("abcXYZdef"s, 12) == "XYZdefabc"s);
}